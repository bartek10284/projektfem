﻿
clear 
clc


            % parametry sterujące

% wlasnocci materialu 
P=1;                       % przewodnoc ciapla paterialu
pc=1;                      % poojemnoc cieplna
A=1;                       % przekroj pręta
units=2;                   % dlugosc preta
s = 1;                     % zrodlo temperatury
        %tworze geometrie

%parametry  potrzebne do utwozena gromertii

h=0.1;                      % dystans między wezami
p=[0:h:units]';             
x = [0:h:units];            % Przestrzenna dyskretyzacja
nr_nodes = length(p);     

% tworze macierz elemetow o rownych elemetach  dlugosci = h
  for i = 2:nr_nodes       
      e(i-1,:) = [i-1 i];
  end
  
  
  %potrzebne do obliczen wartoisci
dt = 0.01;                  % do pochodenk
t=1;                        % inicjalizacja czasu





nr_elements =length(e);     % Nr of elements

    
    K=zeros(nr_nodes,nr_nodes); % pusta macierz sztywnosci 
    M=zeros(nr_nodes,nr_nodes); % pusta macierz mass
    F=zeros(nr_nodes,1);        % prawa strona
    Q=zeros(nr_nodes,1);        % poczatkowa prawa strona 
    
    
    for k = 1:nr_elements                        % przez wszystkie elementy 
    
        nodes=e(k,:);                        % nody
        h=p(k+1)-p(k);                       % roznica miedzi nodami 
        
        Ke = ((A*P)/h)*[1 -1;-1 1];          % sztywnocici
        Me = ((pc*A*h)/6)*[2 1;1 2];         % masy    
        Fe = ((A*h)/6)*[2 1;1 2]*Q(nodes,1); % sil
                            
        K(nodes,nodes)=K(nodes,nodes)+Ke;    % pozycja sztywnoci + node 
        M(nodes,nodes)=M(nodes,nodes)+Me;    % pozycja masy + node    
        F(nodes,1)=F(nodes,1)+Fe;            % pozycja sily(temperatury) + node       
    end       


Told=zeros(nr_nodes,1);    
F(s)=0.5;             %temperatura zrodlo        


dt = 0.01;                 
t=1;                      


A=((1/2)*K+(1/(dt))*M);     
B=(-(1/2)*K+M/(dt));        

C=(M+0.5*dt*K);             
D=(M-0.5*dt*K);             

R=((1/3)*K+(1/(2*dt))*M);   
S=((-1/6)*K+(M/(2*dt)));    
 
R(nr_nodes,:)=0;
R(:,nr_nodes)=0;
R(nr_nodes,nr_nodes)=1;

S(nr_nodes,:)=0;
S(:,nr_nodes)=0;
S(nr_nodes,nr_nodes)=1;

Tnew = Told;
Fold = F;
Fnew = Fold;

for i = 1:t/dt    
    thermometer(i)=Tnew(s);                
   Tnew=R\((S*Told+((1/6)*(Fold+2*Fnew))));    
   Told=Tnew;
end





  figure(1) 
  plot(x,Tnew,'r*',x,Tnew,'b')
  title('Temperature distribution in rod')
  xlabel('Length')
  ylabel('Temperature [C]')
  legend(['Galerkin dt=',num2str(dt)])
  axis([0 units 0 1.4])